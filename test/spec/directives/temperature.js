'use strict';

describe('Directive: Temperature', function () {

  // load the directive's module
  beforeEach(module('angularTestApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-temperature></-temperature>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the Temperature directive');
  }));
});
