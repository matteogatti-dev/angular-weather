'use strict';

describe('Filter: first', function () {

  // load the filter's module
  beforeEach(module('angularTestApp'));

  // initialize a new instance of the filter before each test
  var first;
  beforeEach(inject(function ($filter) {
    first = $filter('first');
  }));

  it('should return the input prefixed with "first filter:"', function () {
    var text = 'angularjs';
    expect(first(text)).toBe('first filter: ' + text);
  });

});
