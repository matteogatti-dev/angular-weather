'use strict';

describe('Filter: stringToArray', function () {

  // load the filter's module
  beforeEach(module('angularTestApp'));

  // initialize a new instance of the filter before each test
  var stringToArray;
  beforeEach(inject(function ($filter) {
    stringToArray = $filter('stringToArray');
  }));

  it('should return the input prefixed with "stringToArray filter:"', function () {
    var text = 'angularjs';
    expect(stringToArray(text)).toBe('stringToArray filter: ' + text);
  });

});
