'use strict';

describe('Filter: stringToDate', function () {

  // load the filter's module
  beforeEach(module('angularTestApp'));

  // initialize a new instance of the filter before each test
  var stringToDate;
  beforeEach(inject(function ($filter) {
    stringToDate = $filter('stringToDate');
  }));

  it('should return the input prefixed with "stringToDate filter:"', function () {
    var text = 'angularjs';
    expect(stringToDate(text)).toBe('stringToDate filter: ' + text);
  });

});
