'use strict';

/**
 * @ngdoc service
 * @name angularTestApp.Weather
 * @description
 * # Weather
 * Service in the angularTestApp.
 */
angular.module('angularTestApp')
  .service('Weather', function (config, $http) {

    this.Search = function (term) {

      return $http({
        method: 'GET',
        url: config.WEATHER_API_HOST,
        params: {
          appid: config.WEATHER_API_KEY,
          q: sprintf('%s,%s', term, config.LOCALE),
          units: config.UNIT,
          lang: config.LOCALE
        }
      })

    };

  });
