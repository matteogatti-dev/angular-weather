'use strict';

/**
 * @ngdoc service
 * @name angularTestApp.City
 * @description
 * # City
 * Service in the angularTestApp.
 */
angular.module('angularTestApp')
  .service('City', function (config, $http) {

    /**
     * @param term  string
     * @constructor
     */
    this.Search = function (term) {

      var url = sprintf('%s?filter=%s&q=%s', config.CITY_API_HOST, config.LOCALE, term);

      return $http.jsonp(url).then(function(response) {
        return response;
      });

    };

  });
