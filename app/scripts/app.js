'use strict';

/**
 * @ngdoc overview
 * @name angularTestApp
 * @description
 * # angularTestApp
 *
 * Main module of the application.
 */
angular
  .module('angularTestApp', [
    'ngAnimate',
    'ngRoute',
    'ngSanitize',
    'sprintf',
    'ngLodash',
    'chart.js'
  ])
  .config(function ($routeProvider, $sceDelegateProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });

    $sceDelegateProvider.resourceUrlWhitelist([
      'self',
      'http://gd.geobytes.com/**'
    ]);
  })
  .value('config', {
    WEATHER_API_KEY: 'd91a598f915755d3e737871d11c23441',
    WEATHER_API_HOST: 'http://api.openweathermap.org/data/2.5/forecast',
    CITY_API_HOST: 'http://gd.geobytes.com/AutoCompleteCity',
    LOCALE: 'IT',
    UNIT: 'metric'
  });
