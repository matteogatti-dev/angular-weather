'use strict';

/**
 * @ngdoc directive
 * @name angularTestApp.directive:pressure
 * @description
 * # pressure
 */
angular.module('angularTestApp')
  .directive('pressure', function () {
    return {
      templateUrl: 'views/directives/pressure.html',
      restrict: 'E',
      transclude : true,
      replace: true,
      scope: {
        weathers: '=data'
      },
      link: function(scope, element, attrs) {

        var today = moment();

        scope.pressures = [];
        scope.labels = [];

        scope.$watch('weathers', function(data) {
          if (data !== undefined){

            angular.forEach(data, function (value, key) {
              if (moment.unix(value.dt).isSame(today, 'day')) {
                scope.pressures.push(value.main.pressure);
                scope.labels.push(moment.unix(value.dt).format('HH:mm'));
              }
            })

          }
        });

      }
    };
  });
