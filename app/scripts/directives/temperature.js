'use strict';

/**
 * @ngdoc directive
 * @name angularTestApp.directive:Temperature
 * @description
 * # Temperature
 */
angular.module('angularTestApp')
  .directive('temperature', function () {
    return {
      templateUrl: 'views/directives/temperature.html',
      restrict: 'E',
      transclude : true,
      replace: true,
      scope: {
        weathers: '=data'
      },
      link: function(scope, element, attrs) {

        var today = moment();

        scope.temperatures = [];
        scope.labels = [];

        scope.$watch('weathers', function(data) {
          if (data !== undefined){

            angular.forEach(data, function (value, key) {
              if (moment.unix(value.dt).isSame(today, 'day')) {
                scope.temperatures.push(value.main.temp);
                scope.labels.push(moment.unix(value.dt).format('HH:mm'));
              }
            })

          }
        });

      }
    };
  });
