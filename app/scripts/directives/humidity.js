'use strict';

/**
 * @ngdoc directive
 * @name angularTestApp.directive:humidity
 * @description
 * # humidity
 */
angular.module('angularTestApp')
  .directive('humidity', function () {
    return {
      templateUrl: 'views/directives/humidity.html',
      restrict: 'E',
      transclude : true,
      replace: true,
      scope: {
        weathers: '=data'
      },
      link: function(scope, element, attrs) {

        var today = moment();

        scope.humidities = [];
        scope.labels = [];

        scope.$watch('weathers', function(data) {
          if (data !== undefined){

            angular.forEach(data, function (value, key) {
              if (moment.unix(value.dt).isSame(today, 'day')) {
                scope.humidities.push(value.main.humidity);
                scope.labels.push(moment.unix(value.dt).format('HH:mm'));
              }
            })

          }
        });

      }
    };
  });
