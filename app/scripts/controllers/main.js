'use strict';

/**
 * @ngdoc function
 * @name angularTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularTestApp
 */
angular.module('angularTestApp')
  .controller('MainCtrl', function ($scope, Weather, City) {

    var doAjax = true;

    $scope.showGraph = false;

    /**
     * @returns {boolean}
     */
    $scope.onFormSubmit = function () {

      if ($scope.term.length === 0) {
        $scope.cities = [];
        doAjax = false;
      }

      if (doAjax) {
        City.Search($scope.term).then(function (response) {
          if (response.status === 200) {
            $scope.cities = response.data;
          }
        });
      }

    };

    $scope.onCitySelect = function (city) {
      $scope.city = city;
      $scope.term = '';
      $scope.cities = [];
      $scope.weathers = [];
      $scope.todayWeathers = [];

      Weather.Search(city).then(function (response) {
        if (response.status === 200) {
          $scope.weathers = response.data.list;

          angular.forEach(response.data.list, function (value, key) {
            if (moment.unix(value.dt).isSame(moment(), 'day')) {
              $scope.todayWeathers.push(value)
            }
          });

          $scope.showGraph = true;
        }
      })
    }

  });
