'use strict';

/**
 * @ngdoc filter
 * @name angularTestApp.filter:capitalize
 * @function
 * @description
 * # capitalize
 * Filter in the angularTestApp.
 */
angular.module('angularTestApp')
  .filter('capitalize', function () {
    return function (input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    };
  });
