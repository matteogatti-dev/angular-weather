'use strict';

/**
 * @ngdoc filter
 * @name angularTestApp.filter:stringToDate
 * @function
 * @description
 * # stringToDate
 * Filter in the angularTestApp.
 */
angular.module('angularTestApp')
  .filter('stringToDate', function () {
    return function (input, format) {
      if (typeof format !== 'undefined') {
        return moment.unix(input).format(format)
      }

      return moment.unix(input)
    };
  });
