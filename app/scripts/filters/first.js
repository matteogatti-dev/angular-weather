'use strict';

/**
 * @ngdoc filter
 * @name angularTestApp.filter:first
 * @function
 * @description
 * # first
 * Filter in the angularTestApp.
 */
angular.module('angularTestApp')
  .filter('first', function () {

    return function (input) {
      if (Array.isArray(input)) {
        return input[0]
      }

      return input;
    };

  });
