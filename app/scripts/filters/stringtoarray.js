'use strict';

/**
 * @ngdoc filter
 * @name angularTestApp.filter:stringToArray
 * @function
 * @description
 * # stringToArray
 * Filter in the angularTestApp.
 */
angular.module('angularTestApp')
  .filter('stringToArray', function () {

    return function (input, separator) {
      if (typeof input === 'string') {

        if (typeof separator === 'undefined') {
          throw new Error("A separator value must be provided!");
        }

        return input.split(separator)
      }

      return input;
    };

  });
